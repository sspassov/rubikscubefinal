package com.example.rubiks.Abstract;

import android.graphics.Color;

import com.example.rubiks.Interfaces.ICorner;

public class AbsCorner implements ICorner {
	
	private final int leftColor;
	private final int topColor;
	private final int rightColor;
	private int cornerPos;
	
	public AbsCorner(int leftColor, int topColor, int rightColor){
		this.leftColor = leftColor;
		this.topColor = topColor;
		this.rightColor = rightColor;
	}

	public int getLeftColor() {
		return leftColor;
	}

	public int getTopColor() {
		return topColor;
	}

	public int getRightColor() {
		return rightColor;
	}

	public String toString(){
		return leftColor + " " + topColor + " " + rightColor;
	}

	public int getCornerPos(){
		return cornerPos;
	}

	public void setCornerPos(int cornerPos){
		this.cornerPos = cornerPos;
	}

	@Override
	public boolean equals(Object obj){
		final AbsCorner corner = (AbsCorner) obj;
		boolean hasCommonColors = this.leftColor == corner.getLeftColor() && this.topColor == corner.getTopColor() && this.rightColor == corner.getRightColor();
		return hasCommonColors;
	}

	public boolean isSameCorner(AbsCorner corner){
		boolean hasCommonLeftColor = leftColor == (corner.leftColor) || leftColor == (corner.topColor) || leftColor == (corner.rightColor);
		boolean hasCommonTopColor = topColor == (corner.leftColor) || topColor == (corner.topColor) || topColor == (corner.rightColor);
		boolean hasCommonRightColor = rightColor == (corner.leftColor) || rightColor == (corner.topColor) || rightColor == (corner.rightColor);
		if (hasCommonLeftColor && hasCommonTopColor && hasCommonRightColor){
			return true;
		}
		return false;
	}

	public boolean equalsWithPos(AbsCorner corner){
		if(this.equals(corner) && cornerPos == corner.cornerPos){
			return true;
		}
		return false;
	}


}
