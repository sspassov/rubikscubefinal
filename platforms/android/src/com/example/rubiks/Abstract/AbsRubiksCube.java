package com.example.rubiks.Abstract;

import com.example.rubiks.Interfaces.IRubiksCube;

public abstract class AbsRubiksCube implements IRubiksCube {

	private final int rows;
	private final int cols;
	private final int numOfSides;
	
	public AbsRubiksCube(int rows, int cols, int numOfSides){
		this.rows = rows;
		this.cols = cols;
		this.numOfSides = numOfSides;
	}
	
	public int getRows(){
		return rows;
	}
	
	public int getCols(){
		return cols;
	}
	
	public int getNumOfSides(){
		return numOfSides;
	}
	

}
