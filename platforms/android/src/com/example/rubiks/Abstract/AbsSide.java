package com.example.rubiks.Abstract;

import com.example.rubiks.Interfaces.ISide;

public class AbsSide implements ISide {
	
	private final String side;
	private final int[] colors;
	
	public AbsSide(String side, int[] colors){
		this.side = side;
		this.colors = colors;
	}
	
	public String getSide(){
		return side;
	}
	
	public int[] getColors(){
		return colors;
	}
}
