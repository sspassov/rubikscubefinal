package com.example.rubiks.Abstract;

import android.graphics.Color;

import com.example.rubiks.Interfaces.IEdge;

/**
 * Created by sspas on 10/6/2016.
 */
public class AbsEdge implements IEdge {

    private final int topColor;
    private final int bottomColor;
    private int edgePos;

    public AbsEdge(int topColor, int bottomColor){
        this.topColor = topColor;
        this.bottomColor = bottomColor;
    }

    public int getTopColor(){
        return topColor;
    }

    public int getBottomColor(){
        return bottomColor;
    }

    public int getEdgePos() {
        return edgePos;
    }

    public void setEdgePos(int edgePos){
        this.edgePos = edgePos;
    }

    @Override
    public boolean equals(Object obj){
        final AbsEdge edge = (AbsEdge) obj;
        if(this.topColor == edge.getTopColor() && this.bottomColor == edge.getBottomColor()){
            return true;
        }
        return false;
    }

    public boolean isSameEdge(AbsEdge edge) {
        boolean hasCommonTopColor = topColor == (edge.topColor) || topColor == (edge.bottomColor);
        boolean hasCommonBottomColor = bottomColor == (edge.bottomColor) || bottomColor == (edge.topColor);
        if (hasCommonTopColor && hasCommonBottomColor){
            return true;
        }
        return false;
    }

    public boolean equalsWithPos(AbsEdge edge){
        // For unit testing only
        if(this.equals(edge) && edgePos == edge.edgePos){
            return true;
        }
        return false;

    }

}
