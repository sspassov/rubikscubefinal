package com.example.rubiks.Interfaces;

public interface IRubiksCube {

    int getRows();
    int getCols();
    int getNumOfSides();
}
