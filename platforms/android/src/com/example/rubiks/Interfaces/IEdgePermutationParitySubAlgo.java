package com.example.rubiks.Interfaces;

import com.example.rubiks.Concrete.Edge3x3;

/**
 * Created by sspas on 10/6/2016.
 */
public interface IEdgePermutationParitySubAlgo {

    Edge3x3[] generateSolvedEdgesWithPositions();

    void generateEdgesWithPositions();

    int getEdgeSwaps();
}
