package com.example.rubiks.Interfaces;

/**
 * Created by sspas on 10/6/2016.
 */
public interface IEdge {

    int getTopColor();
    int getBottomColor();
}
