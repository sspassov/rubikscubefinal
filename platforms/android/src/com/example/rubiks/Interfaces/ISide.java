package com.example.rubiks.Interfaces;

public interface ISide {

    int[] getColors();
    String getSide();
}
