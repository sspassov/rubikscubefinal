package com.example.rubiks.Interfaces;

public interface ICorner {
	
	int getLeftColor();
	int getTopColor();
	int getRightColor();

}
