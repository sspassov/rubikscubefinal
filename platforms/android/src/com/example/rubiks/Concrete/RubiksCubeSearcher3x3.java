package com.example.rubiks.Concrete;

import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;

import com.example.rubiks.twophase.Search;


/**
 * Created by sspas on 1/27/2017.
 * Simple class, which uses the two phase algo library to find an effiecient solution to a solvable cube
 * needs to exist since I need to annotate the methods used by javascript
 */
public class RubiksCubeSearcher3x3 {

    private static Context context;

    public RubiksCubeSearcher3x3(Context c){
        context = c;
    }

    @JavascriptInterface
    public static String solutionSearch(String facelets, int maxDepth, long timeOut, boolean useSeparator){
        return Search.solution(facelets, 50, 5, useSeparator, context);
    }

}
