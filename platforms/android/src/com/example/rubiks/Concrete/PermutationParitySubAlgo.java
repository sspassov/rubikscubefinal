package com.example.rubiks.Concrete;

/**
 * Created by sspas on 10/6/2016.
 */
public class PermutationParitySubAlgo {

    public static boolean checkPermutationParity(RubiksCube3x3 cube){
        EdgePermutationParitySubAlgo edgePermutationAlgo = new EdgePermutationParitySubAlgo(cube);
        int edgeSwaps = edgePermutationAlgo.getEdgeSwaps();
        CornerPermutationParitySubAlgo cornerPermutationAlgo = new CornerPermutationParitySubAlgo(cube);
        int cornerSwaps = cornerPermutationAlgo.getCornerSwaps();

        if((edgeSwaps+cornerSwaps) % 2 == 0){
            return true;
        }
        return false;

    }
}
