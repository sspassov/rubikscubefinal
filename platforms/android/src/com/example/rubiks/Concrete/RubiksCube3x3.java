package com.example.rubiks.Concrete;

import android.graphics.Color;

import com.example.rubiks.Abstract.AbsRubiksCube;

public class RubiksCube3x3 extends AbsRubiksCube {
	
	private Side3x3 topSide;
	private Side3x3 bottomSide;
	private Side3x3 frontSide;
	private Side3x3 backSide;
	private Side3x3 leftSide;
	private Side3x3 rightSide;
	private Corner3x3[] corners = new Corner3x3[8];
	private Edge3x3[] edges = new Edge3x3[12];

	public RubiksCube3x3(int[] topColors,int[] bottomColors,int[] frontColors,int[] backColors,int[] leftColors,int[] rightColors) {
		super(3, 3, 6);
		generateSides(topColors,bottomColors,frontColors,backColors,leftColors,rightColors);
		generateCorners();
		generateEdges();
	}
	
	private void generateSides(int[] topColors,int[] bottomColors,int[] frontColors,int[] backColors,int[] leftColors,int[] rightColors){
		topSide = new Side3x3("top", topColors);
		bottomSide = new Side3x3("bottom", bottomColors);
		frontSide = new Side3x3("front", frontColors);
		backSide = new Side3x3("back", backColors);
		leftSide = new Side3x3("left", leftColors);
		rightSide = new Side3x3("right", rightColors);
	}
	
	private void generateCorners(){
		generateTopSideCorners();
		generateBottomSideCorners();
	}

	private void generateBottomSideCorners() {
		// Bottom Side3x3 Corners
		Corner3x3 bottomSideTopLeftCorner = new Corner3x3(leftSide.getColors()[8], bottomSide.getColors()[0], backSide.getColors()[2]);
		corners[4] = bottomSideTopLeftCorner;
		Corner3x3 bottomSideTopRightCorner = new Corner3x3(backSide.getColors()[8], bottomSide.getColors()[6], rightSide.getColors()[0]);
		corners[5] = bottomSideTopRightCorner;
		Corner3x3 bottomSideBottomLeftCorner = new Corner3x3(frontSide.getColors()[2], bottomSide.getColors()[2], leftSide.getColors()[2]);
		corners[6] = bottomSideBottomLeftCorner;
		Corner3x3 bottomSideBottomRightCorner = new Corner3x3(rightSide.getColors()[2], bottomSide.getColors()[8], frontSide.getColors()[0]);
		corners[7] = bottomSideBottomRightCorner;
	}

	private void generateTopSideCorners() {
		// Top Side3x3 Corners
		Corner3x3 topSideTopLeftCorner = new Corner3x3(backSide.getColors()[0], topSide.getColors()[8], leftSide.getColors()[6]);
		corners[0] = topSideTopLeftCorner;
		Corner3x3 topSideTopRightCorner = new Corner3x3(rightSide.getColors()[6], topSide.getColors()[6], backSide.getColors()[6]);
		corners[1] = topSideTopRightCorner;
		Corner3x3 topSideBottomLeftCorner = new Corner3x3(leftSide.getColors()[0], topSide.getColors()[2], frontSide.getColors()[8]);
		corners[2] = topSideBottomLeftCorner;
		Corner3x3 topSideBottomRightCorner = new Corner3x3(frontSide.getColors()[6], topSide.getColors()[0], rightSide.getColors()[8]);
		corners[3] = topSideBottomRightCorner;
	}

	private void generateEdges(){
		generateTopSideEdges();
		generateMiddleSideEdges();
		generateBottomSideEdges();
	}

	private void generateBottomSideEdges() {
		//Bottom Edges
		Edge3x3 bottomFrontEdge = new Edge3x3(bottomSide.getColors()[5], frontSide.getColors()[1]);
		edges[8] = bottomFrontEdge;
		Edge3x3 bottomRightEdge = new Edge3x3(bottomSide.getColors()[7], rightSide.getColors()[1]);
		edges[9] = bottomRightEdge;
		Edge3x3 bottomBackEdge = new Edge3x3(bottomSide.getColors()[3], backSide.getColors()[5]);
		edges[10] = bottomBackEdge;
		Edge3x3 bottomLeftEdge = new Edge3x3(bottomSide.getColors()[1], leftSide.getColors()[5]);
		edges[11] = bottomLeftEdge;
	}

	private void generateMiddleSideEdges() {
		//Middle Edges
		Edge3x3 frontRightEdge = new Edge3x3(frontSide.getColors()[3], rightSide.getColors()[5]);
		edges[4] = frontRightEdge;
		Edge3x3 rightBackEdge = new Edge3x3(backSide.getColors()[7], rightSide.getColors()[3]);
		edges[5] = rightBackEdge;
		Edge3x3 backLeftEdge = new Edge3x3(backSide.getColors()[1], leftSide.getColors()[7]);
		edges[6] = backLeftEdge;
		Edge3x3 leftFrontEdge = new Edge3x3(frontSide.getColors()[5], leftSide.getColors()[1]);
		edges[7] = leftFrontEdge;
	}

	private void generateTopSideEdges() {
		//Top Edges
		Edge3x3 topFrontEdge = new Edge3x3(topSide.getColors()[1], frontSide.getColors()[7]);
		edges[0] = topFrontEdge;
		Edge3x3 topRightEdge = new Edge3x3(topSide.getColors()[3], rightSide.getColors()[7]);
		edges[1] = topRightEdge;
		Edge3x3 topBackEdge = new Edge3x3(topSide.getColors()[7], backSide.getColors()[3]);
		edges[2] = topBackEdge;
		Edge3x3 topLeftEdge = new Edge3x3(topSide.getColors()[5], leftSide.getColors()[3]);
		edges[3] = topLeftEdge;
	}

	public Corner3x3[] getCorners() {
		return corners;
	}

	public Edge3x3[] getEdges() { return edges; }
	
	public Side3x3 getTopSide(){
		return topSide;
	}
	
	public Side3x3 getBottomSide(){
		return bottomSide;
	}

	public Side3x3 getFrontSide() { return frontSide; }

	public Side3x3 getBackSide() { return backSide; }

	public Side3x3 getLeftSide() { return leftSide; }

	public Side3x3 getRightSide() { return rightSide; }

	public void countEdges(){
		int counter = 0;
		for (int i = 0; i < edges.length; i++){
			if (edges[i].getBottomColor() == (topSide.getSideColor()) ||edges[i].getBottomColor() == (bottomSide.getSideColor())){
				counter++;
			}else if(edges[i].getBottomColor() == (frontSide.getSideColor()) ||edges[i].getBottomColor() == (backSide.getSideColor())){
				if(edges[i].getTopColor() != (topSide.getSideColor()) && edges[i].getTopColor() != (bottomSide.getSideColor())) {
					counter++;
				}
			}
		}
		System.out.println(counter + "");
	}

	
	

}
