package com.example.rubiks.Concrete;

import android.graphics.Color;

import com.example.rubiks.Abstract.AbsSide;

public class Side3x3 extends AbsSide {
	
	private final int sideColor;

	public Side3x3(String side, int[] colors) {
		super(side, colors);
		sideColor = colors[4];
	}
	
	public int getSideColor(){
		return sideColor;
	}

}
