package com.example.rubiks.Concrete;

/**
 * Created by sspas on 10/15/2016.
 */
public class EdgeParitySubAlgo {
    
    public static boolean checkEdgeParity(RubiksCube3x3 cube){
        int edgeSwaps = getEdgeSwaps(cube);
        if (edgeSwaps % 2 == 0){
            return true;
        }
        return false;
    }

    public static int getEdgeSwaps(RubiksCube3x3 cube){
        int edgeSwaps = 0;
        for (int i = 0; i < cube.getEdges().length; i++){
            if ( cube.getEdges()[i].getBottomColor() == (cube.getTopSide().getSideColor()) || cube.getEdges()[i].getBottomColor() == (cube.getBottomSide().getSideColor())){
                edgeSwaps++;
            }else if( cube.getEdges()[i].getBottomColor() == (cube.getFrontSide().getSideColor()) || cube.getEdges()[i].getBottomColor() == (cube.getBackSide().getSideColor())){
                if(cube.getEdges()[i].getTopColor() != (cube.getTopSide().getSideColor()) && cube.getEdges()[i].getTopColor() != (cube.getBottomSide().getSideColor())) {
                    edgeSwaps++;
                }
            }
        }
        return edgeSwaps;
    }
}
