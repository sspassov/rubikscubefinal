package com.example.rubiks.Concrete;

public class CornerParitySubAlgo {

	public static boolean checkCornerParity(RubiksCube3x3 cube){
		int cornerSum = 0;
		for (int i = 0; i < cube.getCorners().length; i++){
			if (cube.getCorners()[i].getLeftColor() == cube.getTopSide().getSideColor() || cube.getCorners()[i].getLeftColor() == cube.getBottomSide().getSideColor()){
				cornerSum += 2;
			}else if(cube.getCorners()[i].getRightColor() == cube.getTopSide().getSideColor() || cube.getCorners()[i].getRightColor() == cube.getBottomSide().getSideColor()){
				cornerSum += 1;
			}
		}
		if (cornerSum % 3 == 0){
			return true;
		}
		return false;
	}
}
