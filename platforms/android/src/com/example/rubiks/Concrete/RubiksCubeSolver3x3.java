package com.example.rubiks.Concrete;

import android.graphics.Color;
import android.util.Log;
import android.webkit.JavascriptInterface;

import com.example.rubiks.Abstract.AbsRubiksCubeSolver;

/**
 * Created by sspas on 11/17/2016.
 */
public class RubiksCubeSolver3x3 extends AbsRubiksCubeSolver{

    private int colorsSize = 0;
    private int redCount = 0;
    private int greenCount = 0;
    private int blueCount = 0;
    private int whiteCount = 0;
    private int yellowCount = 0;

    @JavascriptInterface
    @Override
    public boolean isRubiksCubeSolvable(String[] topColors, String[] bottomColors, String[] frontColors, String[] backColors, String[] leftColors, String[] rightColors) {
        int[][] allColorsAsInts = convertStringColorsToInt(topColors,bottomColors,frontColors,backColors,leftColors,rightColors);
        if(allColorsAsInts == null){
            return false;
        }
        RubiksCube3x3 cube = new RubiksCube3x3(allColorsAsInts[0], allColorsAsInts[1], allColorsAsInts[2], allColorsAsInts[3], allColorsAsInts[4], allColorsAsInts[5]);
        boolean permutationParityPasses = PermutationParitySubAlgo.checkPermutationParity(cube);
        boolean cornerParityPasses = CornerParitySubAlgo.checkCornerParity(cube);
        boolean edgeParityPasses = EdgeParitySubAlgo.checkEdgeParity(cube);
        if(permutationParityPasses && cornerParityPasses && edgeParityPasses){
            return true;
        }
        return false;
    }

    private int[][] convertStringColorsToInt(String[] topColors, String[] bottomColors, String[] frontColors, String[] backColors, String[] leftColors, String[] rightColors) {
        //Method which converts the colors from javascript to Color ints
        int numOfSides = 6;
        colorsSize = topColors.length;
        String[][] allColors = new String[numOfSides][topColors.length];
        int[][] allColorsAsInts = new int[numOfSides][topColors.length];
        allColors[0] = topColors;
        allColors[1] = bottomColors;
        allColors[2] = frontColors;
        allColors[3] = backColors;
        allColors[4] = leftColors;
        allColors[5] = rightColors;
        resetColorCount();
        for (int i = 0; i < numOfSides; i++) {
            for (int j = 0; j < colorsSize; j++) {
                generateColorsInt(allColors[i][j], allColorsAsInts, i, j);
            }
        }
        boolean areColorsTheSameCount = redCount == colorsSize && greenCount == colorsSize && blueCount == colorsSize && whiteCount == colorsSize && yellowCount == colorsSize;
        if(areColorsTheSameCount){
            return allColorsAsInts;
        }
        return null;


    }

    private void resetColorCount() {
        redCount = 0;
        greenCount = 0;
        blueCount = 0;
        whiteCount = 0;
        yellowCount = 0;
    }

    private void generateColorsInt(String s, int[][] allColorsAsInts, int i, int j) {
        if (s.trim().equals("red")) {
            allColorsAsInts[i][j] = Color.RED;
            redCount++;
        } else if (s.trim().equals("green")) {
            allColorsAsInts[i][j] = Color.GREEN;
            greenCount++;
        } else if (s.trim().equals("blue")) {
            allColorsAsInts[i][j] = Color.BLUE;
            blueCount++;
        } else if (s.trim().equals("white")) {
            allColorsAsInts[i][j] = Color.WHITE;
            whiteCount++;
        } else if (s.trim().equals("yellow")) {
            allColorsAsInts[i][j] = Color.YELLOW;
            yellowCount++;
        } else if (s.trim().equals("orange")) {
            //Since android doesn't have a defined type for orange I have to menually specify RGB
            //No need for greenCount
            allColorsAsInts[i][j] = Color.rgb(255, 165, 0);
        }
    }


}
