package com.example.rubiks.twophase;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by sspas on 3/22/2017.
 */
public class FileReader {

    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Reads a txt move table file inside a moveArray Table - Faster than generating the table on the go
    public static void readMoveTableToArray(short[][] moveArray, int rowSize, int colSize, String fileName, Context context){
        try {
            String[] neshto = context.getAssets().list("");
            BufferedReader r = new BufferedReader(new InputStreamReader(context.getAssets().open(fileName)));

            String line;
            int i = 0;
            int j = 0;
            while ((line = r.readLine()) != null) {
                moveArray[i][j] = Short.parseShort(line);
                j++;
                if(j == colSize){
                    i++;
                    j = 0;
                    if( i == rowSize){
                        break;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Reads a txt prun table file inside a prunArray Table - Faster than generating the table on the go
    public static void readPrunTableToArray(byte[] prunArray, int tableSize, String fileName, Context context){
        try {
            BufferedReader r = new BufferedReader(new InputStreamReader(context.getAssets().open(fileName)));
            String line;
            int count = 0;
            while ((line = r.readLine()) != null) {
                prunArray[count] = Byte.parseByte(line);
                count++;
                if(count == tableSize){
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
