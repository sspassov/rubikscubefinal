package androidTest;

import android.content.Context;
import android.test.InstrumentationTestCase;
import com.example.rubiks.twophase.FileReader;

/**
 * Created by sspas on 3/23/2017.
 * Test was created to test the only class that is used in the Two Phase Library
 * This is an Android Test and not a JUnit test since the class requires a context
 */
public class FileReaderTest extends InstrumentationTestCase{

    Context context;

    public void setUp() throws Exception {
        super.setUp();
        context = getInstrumentation().getContext();
        assertNotNull(context);

    }

    public void testReadMoveTableToArray(){
        short[][] expectedArray = new short[2][2];
        expectedArray[0] = new short[] {1, 2};
        expectedArray[1] = new short[] {3, 4};
        short[][] actualArray = new short[2][2];
        FileReader.readMoveTableToArray(actualArray, 2, 2, "tableTest.txt", context);
        for(int i=0; i<expectedArray.length; i++){
            for(int j=0; j < expectedArray.length; j++){
                assertEquals(expectedArray[i][j], actualArray[i][j]);
            }
        }

    }

    public void testReadPrunTableToArray(){
        byte[] expectedArray = new byte[4];
        expectedArray[0] = 1;
        expectedArray[1] = 2;
        expectedArray[2] = 3;
        expectedArray[3] = 4;
        byte[] actualArray = new byte[4];
        FileReader.readPrunTableToArray(actualArray, 4, "tableTest.txt", context);
        for(int i=0; i<expectedArray.length; i++){
            assertEquals(expectedArray[i], actualArray[i]);
        }

    }


}
