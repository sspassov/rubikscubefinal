package test.JUnit;

import android.graphics.Color;

import com.example.rubiks.Concrete.PermutationParitySubAlgo;
import com.example.rubiks.Concrete.RubiksCube3x3;

import junit.framework.TestCase;


/**
 * Created by sspas on 10/7/2016.
 */
public class PermutationParitySubAlgoTest extends TestCase{


    public void testCheckPermutationParityTrue(){
        int[] topColors = new int[]{Color.WHITE, Color.rgb(255, 165, 0), Color.YELLOW, Color.RED, Color.BLUE, Color.YELLOW, Color.YELLOW, Color.rgb(255, 165, 0), Color.rgb(255, 165, 0)};
        int[] bottomColors = new int[]{Color.rgb(255, 165, 0), Color.BLUE, Color.YELLOW, Color.WHITE, Color.GREEN, Color.YELLOW, Color.RED, Color.RED, Color.WHITE};
        int[] frontColors = new int[]{Color.RED, Color.BLUE, Color.RED, Color.RED, Color.WHITE, Color.YELLOW, Color.BLUE, Color.WHITE, Color.RED};
        int[] backColors = new int[]{Color.BLUE, Color.GREEN, Color.WHITE, Color.GREEN, Color.YELLOW, Color.BLUE, Color.rgb(255, 165, 0), Color.YELLOW, Color.BLUE};
        int[] leftColors = new int[]{Color.BLUE, Color.GREEN, Color.GREEN, Color.RED, Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.YELLOW, Color.WHITE, Color.GREEN};
        int[] rightColors = new int[]{Color.WHITE, Color.BLUE, Color.GREEN, Color.rgb(255, 165, 0), Color.RED, Color.GREEN, Color.GREEN, Color.WHITE, Color.rgb(255, 165, 0)};
        RubiksCube3x3 cube = new RubiksCube3x3(topColors, bottomColors, frontColors, backColors, leftColors, rightColors);
        PermutationParitySubAlgo algo = new PermutationParitySubAlgo();
        assertTrue(algo.checkPermutationParity(cube));
    }


    public void testCheckPermutationParityFalse(){
		int[] topColors = new int[]{Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE};
		int[] bottomColors = new int[]{Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN};
		int[] frontColors = new int[]{Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.rgb(255, 165, 0), Color.YELLOW};
		int[] backColors = new int[]{Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE};
		int[] leftColors = new int[]{Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED};
		int[] rightColors = new int[]{Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.YELLOW, Color.rgb(255, 165, 0)};
        RubiksCube3x3 cube = new RubiksCube3x3(topColors, bottomColors, frontColors, backColors, leftColors, rightColors);
        PermutationParitySubAlgo algo = new PermutationParitySubAlgo();
        assertFalse(algo.checkPermutationParity(cube));
    }
}
