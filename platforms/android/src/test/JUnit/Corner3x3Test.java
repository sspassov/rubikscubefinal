package test.JUnit;

import android.graphics.Color;

import com.example.rubiks.Concrete.Corner3x3;

import junit.framework.TestCase;

/**
 * Created by sspas on 10/7/2016.
 */
public class Corner3x3Test extends TestCase{


    public void testTwoCornersAreEqual(){
        Corner3x3 firstCorner = new Corner3x3(Color.RED, Color.BLUE, Color.YELLOW);
        Corner3x3 secondCorner = new Corner3x3(Color.RED, Color.BLUE, Color.YELLOW);
        assertTrue(firstCorner.equals(secondCorner));
    }


    public void testTwoCornersAreNotEqual(){
        Corner3x3 firstCorner = new Corner3x3(Color.RED, Color.BLUE, Color.YELLOW);
        Corner3x3 secondCorner = new Corner3x3(Color.GREEN, Color.BLUE, Color.YELLOW);
        assertFalse(firstCorner.equals(secondCorner));
    }


    public void testTwoCornersAreSame(){
        Corner3x3 firstCorner = new Corner3x3(Color.YELLOW, Color.BLUE, Color.RED);
        Corner3x3 secondCorner = new Corner3x3(Color.RED, Color.BLUE, Color.YELLOW);
        assertTrue(firstCorner.isSameCorner(secondCorner));
    }


    public void testTwoCornersAreNotSame(){
        Corner3x3 firstCorner = new Corner3x3(Color.YELLOW, Color.GREEN, Color.RED);
        Corner3x3 secondCorner = new Corner3x3(Color.RED, Color.BLUE, Color.YELLOW);
        assertFalse(firstCorner.isSameCorner(secondCorner));
    }
}
