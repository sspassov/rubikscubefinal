package test.JUnit;

import android.graphics.Color;

import com.example.rubiks.Concrete.Side3x3;

import junit.framework.TestCase;

/**
 * Created by sspas on 4/9/2017.
 */
public class Side3x3Test extends TestCase {

    Side3x3 side = new Side3x3("blue", new int[] {Color.RED, Color.GREEN, Color.BLUE, Color.WHITE, Color.BLUE, Color.BLUE, Color.WHITE, Color.WHITE, Color.RED});

    public void testGetSide(){
        assertEquals("blue", side.getSide());
    }

    public void testGetColors(){
        int[] expectedOutput = new int[]{Color.RED, Color.GREEN, Color.BLUE, Color.WHITE, Color.BLUE, Color.BLUE, Color.WHITE, Color.WHITE, Color.RED};
        int[] actualOutput = side.getColors();
        for(int i = 0; i < expectedOutput.length; i++){
            assertEquals(expectedOutput[i], actualOutput[i]);
        }
    }

    public void testGetSideColor(){
        assertEquals(Color.BLUE, side.getSideColor());
    }



}
