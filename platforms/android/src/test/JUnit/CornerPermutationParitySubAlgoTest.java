package test.JUnit;

import android.graphics.Color;

import com.example.rubiks.Concrete.Corner3x3;
import com.example.rubiks.Concrete.CornerPermutationParitySubAlgo;
import com.example.rubiks.Concrete.RubiksCube3x3;

import junit.framework.TestCase;

import java.util.Arrays;



/**
 * Created by sspas on 10/11/2016.
 */
public class CornerPermutationParitySubAlgoTest extends TestCase{

    int[] topColors = new int[]{Color.WHITE, Color.rgb(255, 165, 0), Color.YELLOW, Color.RED, Color.BLUE, Color.YELLOW, Color.YELLOW, Color.rgb(255, 165, 0), Color.rgb(255, 165, 0)};
    int[] bottomColors = new int[]{Color.rgb(255, 165, 0), Color.BLUE, Color.YELLOW, Color.WHITE, Color.GREEN, Color.YELLOW, Color.RED, Color.RED, Color.WHITE};
    int[] frontColors = new int[]{Color.RED, Color.BLUE, Color.RED, Color.RED, Color.WHITE, Color.YELLOW, Color.BLUE, Color.WHITE, Color.RED};
    int[] backColors = new int[]{Color.BLUE, Color.GREEN, Color.WHITE, Color.GREEN, Color.YELLOW, Color.BLUE, Color.rgb(255, 165, 0), Color.YELLOW, Color.BLUE};
    int[] leftColors = new int[]{Color.BLUE, Color.GREEN, Color.GREEN, Color.RED, Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.YELLOW, Color.WHITE, Color.GREEN};
    int[] rightColors = new int[]{Color.WHITE, Color.BLUE, Color.GREEN, Color.rgb(255, 165, 0), Color.RED, Color.GREEN, Color.GREEN, Color.WHITE, Color.rgb(255, 165, 0)};
    RubiksCube3x3 cube = new RubiksCube3x3(topColors, bottomColors, frontColors, backColors, leftColors, rightColors);
    CornerPermutationParitySubAlgo algo = new CornerPermutationParitySubAlgo(cube);


    public void testGenerateSolvedCornersWithPositions(){
        Corner3x3[] expectedSolvedCorners = new Corner3x3[8];
        Corner3x3 topSideTopLeftCorner = new Corner3x3(Color.YELLOW, Color.BLUE, Color.rgb(255, 165, 0));
        expectedSolvedCorners[0] = topSideTopLeftCorner;
        Corner3x3 topSideTopRightCorner = new Corner3x3(Color.RED, Color.BLUE, Color.YELLOW);
        expectedSolvedCorners[1] = topSideTopRightCorner;
        Corner3x3 topSideBottomLeftCorner = new Corner3x3(Color.rgb(255, 165, 0), Color.BLUE, Color.WHITE);
        expectedSolvedCorners[2] = topSideBottomLeftCorner;
        Corner3x3 topSideBottomRightCorner = new Corner3x3(Color.WHITE, Color.BLUE, Color.RED);
        expectedSolvedCorners[3] = topSideBottomRightCorner;
        Corner3x3 bottomSideTopLeftCorner = new Corner3x3(Color.rgb(255, 165, 0), Color.GREEN, Color.YELLOW);
        expectedSolvedCorners[4] = bottomSideTopLeftCorner;
        Corner3x3 bottomSideTopRightCorner = new Corner3x3(Color.YELLOW, Color.GREEN, Color.RED);
        expectedSolvedCorners[5] = bottomSideTopRightCorner;
        Corner3x3 bottomSideBottomLeftCorner = new Corner3x3(Color.WHITE, Color.GREEN, Color.rgb(255, 165, 0));
        expectedSolvedCorners[6] = bottomSideBottomLeftCorner;
        Corner3x3 bottomSideBottomRightCorner = new Corner3x3(Color.RED, Color.GREEN, Color.WHITE);
        expectedSolvedCorners[7] = bottomSideBottomRightCorner;
        Corner3x3[] actualSolvedCorners = algo.generateSolvedCornersWithPositions();
        assertTrue(Arrays.equals(expectedSolvedCorners, actualSolvedCorners));


    }


    public void testGenerateCornersWithPositions(){
        Corner3x3[] expectedCornersWithPositions = new Corner3x3[8];
        Corner3x3 topSideTopLeftCorner = new Corner3x3(Color.BLUE, Color.rgb(255, 165, 0), Color.YELLOW);
        topSideTopLeftCorner.setCornerPos(0);
        expectedCornersWithPositions[0] = topSideTopLeftCorner;
        Corner3x3 topSideTopRightCorner = new Corner3x3(Color.GREEN, Color.YELLOW, Color.rgb(255, 165, 0));
        topSideTopRightCorner.setCornerPos(4);
        expectedCornersWithPositions[1] = topSideTopRightCorner;
        Corner3x3 topSideBottomLeftCorner = new Corner3x3(Color.BLUE, Color.YELLOW, Color.RED);
        topSideBottomLeftCorner.setCornerPos(1);
        expectedCornersWithPositions[2] = topSideBottomLeftCorner;
        Corner3x3 topSideBottomRightCorner = new Corner3x3(Color.BLUE, Color.WHITE, Color.rgb(255, 165, 0));
        topSideBottomRightCorner.setCornerPos(2);
        expectedCornersWithPositions[3] = topSideBottomRightCorner;
        Corner3x3 bottomSideTopLeftCorner = new Corner3x3(Color.GREEN, Color.rgb(255, 165, 0), Color.WHITE);
        bottomSideTopLeftCorner.setCornerPos(6);
        expectedCornersWithPositions[4] = bottomSideTopLeftCorner;
        Corner3x3 bottomSideTopRightCorner = new Corner3x3(Color.BLUE, Color.RED, Color.WHITE);
        bottomSideTopRightCorner.setCornerPos(3);
        expectedCornersWithPositions[5] = bottomSideTopRightCorner;
        Corner3x3 bottomSideBottomLeftCorner = new Corner3x3(Color.RED, Color.YELLOW, Color.GREEN);
        bottomSideBottomLeftCorner.setCornerPos(5);
        expectedCornersWithPositions[6] = bottomSideBottomLeftCorner;
        Corner3x3 bottomSideBottomRightCorner = new Corner3x3(Color.GREEN, Color.WHITE, Color.RED);
        bottomSideBottomRightCorner.setCornerPos(7);
        expectedCornersWithPositions[7] = bottomSideBottomRightCorner;
        algo.generateCornersWithPositions();
        for (int i = 0; i < expectedCornersWithPositions.length; i++){
            Corner3x3 actualCorner = cube.getCorners()[i];
            assertTrue(expectedCornersWithPositions[i].equalsWithPos(actualCorner));
        }

    }


    public void testGetEdgeSwaps(){
        assertEquals(5, algo.getCornerSwaps());
    }
}
