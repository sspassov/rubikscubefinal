package test.JUnit;

import android.graphics.Color;

import com.example.rubiks.Concrete.CornerParitySubAlgo;
import com.example.rubiks.Concrete.RubiksCube3x3;

import junit.framework.TestCase;

/**
 * Created by sspas on 10/5/2016.
 */
public class CornerParitySubAlgoTest extends TestCase{


    public void testCheckCornerParityTestTrue(){
        int[] topColors = new int[]{Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE};
        int[] bottomColors = new int[]{Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN};
        int[] frontColors = new int[]{Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW};
        int[] backColors = new int[]{Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE};
        int[] leftColors = new int[]{Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED};
        int[] rightColors = new int[]{Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0)};
        RubiksCube3x3 cube = new RubiksCube3x3(topColors, bottomColors, frontColors, backColors, leftColors, rightColors);
        CornerParitySubAlgo algo = new CornerParitySubAlgo();
        assertTrue(algo.checkCornerParity(cube));
    }


    public void testCheckCornerParityTestFalse(){
        int[] topColors = new int[]{Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.YELLOW};
        int[] bottomColors = new int[]{Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN};
        int[] frontColors = new int[]{Color.YELLOW, Color.YELLOW, Color.rgb(255, 165, 0), Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW};
        int[] backColors = new int[]{Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE};
        int[] leftColors = new int[]{Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED};
        int[] rightColors = new int[]{Color.BLUE, Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0)};
        RubiksCube3x3 cube = new RubiksCube3x3(topColors, bottomColors, frontColors, backColors, leftColors, rightColors);
        CornerParitySubAlgo algo = new CornerParitySubAlgo();
        assertFalse(algo.checkCornerParity(cube));
    }
}
