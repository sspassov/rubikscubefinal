window.cube;
var firstTime = true;
var currentR = 255;
var currentG = 255;
var currentB = 255;
var r = currentR;
var g = currentG;
var b = currentB;
var gEvent;
$(document).ready( function(){

	document.addEventListener('touchmove', function(e) { e.preventDefault(); }, false);
	
	var controls = ERNO.Locked;

	var ua = navigator.userAgent,
		isIE = ua.indexOf('MSIE') > -1 || ua.indexOf('Trident/') > -1;

	window.cube = new ERNO.Cube({
		hideInvisibleFaces: true,
		controls: controls,
		renderer: isIE?ERNO.renderers.IeCSS3D : null

	});


	cube.addEventListener('click', function( event ){
		var radios = document.getElementsByName('color');
		var stickerColor = '';
		for(var i = 0; i < radios.length; i++){
			if (radios[i].checked){
				stickerColor = radios[i].id.toString();
			}
		}
		if(stickerColor != ''){
			var cubelet = event.detail.cubelet;
			if(cubelet.type != "center"){
				var face = event.detail.face;
				face = ( face === 'TOP' ) ? 'UP' : face
				var element = cubelet [face.toLowerCase()].element;
				var sticker = 'sticker ' + stickerColor;
				element.querySelector('.sticker').className = sticker;
			}
		}
	});

	cube.camera.setLens(30);

	var container = document.getElementById('container');
	container.appendChild(cube.domElement);
});


function radioClicked(){
	if(gEvent != null){
		clearInterval(gEvent)
	}
	var radios = document.getElementsByName('color');
	var stickerColor = '';
		for(var i = 0; i < radios.length; i++){
			if (radios[i].checked){
				stickerColor = radios[i].id.toString();
			}
		}

	if(stickerColor=="white"){
    	gEvent=setInterval("colorTransition(255,255,255,false);", 10);		
	}else if(stickerColor=="blue"){
		gEvent=setInterval("colorTransition(50,180,220,false);", 10);
	}else if(stickerColor=="green"){
		gEvent=setInterval("colorTransition(45,205,115,false);", 10);
	}else if(stickerColor=="red"){
		gEvent=setInterval("colorTransition(230,75,60,false);", 10);
	}else if(stickerColor=="orange"){
		gEvent=setInterval("colorTransition(245,155,20,false);", 10);
	}else if(stickerColor=="yellow"){
		gEvent=setInterval("colorTransition(245,225,25,false);", 10);
	}

}

function solveCube(){
	//Getting the top colors
	var topColors = [];
	for (var i = 0; i < 9; i++){
		topColors[i] = window.cube.faces[1].cubelets[i].up.element.querySelector('.sticker').className.split(' ')[1];
	}
	var bottomColors = [];
	for (var i = 0; i < 9; i++){
		bottomColors[i] = window.cube.faces[3].cubelets[i].down.element.querySelector('.sticker').className.split(' ')[1];
	}
	var frontColors = [];
	for (var i = 0; i < 9; i++){
		frontColors[i] = window.cube.faces[0].cubelets[i].front.element.querySelector('.sticker').className.split(' ')[1];
	}
	var backColors = [];
	for (var i = 0; i < 9; i++){
		backColors[i] = window.cube.faces[5].cubelets[i].back.element.querySelector('.sticker').className.split(' ')[1];
	}
	var leftColors = [];
	for (var i = 0; i < 9; i++){
		leftColors[i] = window.cube.faces[4].cubelets[i].left.element.querySelector('.sticker').className.split(' ')[1];
	}
	var rightColors = [];
	for (var i = 0; i < 9; i++){
		rightColors[i] = window.cube.faces[2].cubelets[i].right.element.querySelector('.sticker').className.split(' ')[1];
	}
	if(window.Solver.isRubiksCubeSolvable(topColors, bottomColors, frontColors, backColors, leftColors, rightColors)){
		searchForSolution(topColors, bottomColors, frontColors, backColors, leftColors, rightColors);

	}else{
		var message = document.getElementById('outputMessage');
			message.innerHTML = "The cube you have created is not solvable";
			r = currentR;
			g = currentG;
			b = currentB;
			clearInterval(gEvent);
			gEvent=setInterval("colorTransition(230,75,60, true);", 3);
			$("#container").effect("shake");

	}

	
}

function searchForSolution(topColors, bottomColors, frontColors, backColors, leftColors, rightColors){
	//Identifying the color of each side 
	//Cannot do that from the cube color property since it doesn't change when you rotate the cube
	
	topColor = topColors[4];
	bottomColor = bottomColors[4];
	frontColor = frontColors[4];
	backColor = backColors[4];
	leftColor = leftColors[4];
	rightColor = rightColors[4];
	allColors = [topColor, bottomColor, frontColor, backColor, leftColor, rightColor];
	test = transformColorsToSidesComplex(leftColors, allColors, true);
	sideNotationFaceles = "";
	sideNotationFaceles = sideNotationFaceles.concat(transformColorsToSidesStandard(topColors, allColors));
	sideNotationFaceles = sideNotationFaceles.concat(transformColorsToSidesStandard(rightColors, allColors));
	sideNotationFaceles = sideNotationFaceles.concat(transformColorsToSidesStandard(frontColors, allColors));
	sideNotationFaceles = sideNotationFaceles.concat(transformColorsToSidesComplex(bottomColors, allColors, false));
	sideNotationFaceles = sideNotationFaceles.concat(transformColorsToSidesComplex(leftColors, allColors, true));
	sideNotationFaceles = sideNotationFaceles.concat(transformColorsToSidesComplex(backColors, allColors, true));
	
	if(sideNotationFaceles.length == 54){
			var message = document.getElementById('outputMessage');
			message.innerHTML = "";
			var solution = window.Searcher.solutionSearch(sideNotationFaceles, 30, 10, false);
			if(solution.split(" ")[0] == "Error"){
				message.innerHTML = "The cube you have created is not solvable";
				r = currentR;
				g = currentG;
				b = currentB;
				clearInterval(gEvent);
				gEvent=setInterval("colorTransition(230,75,60, true);", 3);
				$("#container").effect("shake");
			}else{
				message.innerHTML = solution;
			}
	}	
}

//Transforms top colors to side notation
//used for the twophase library
//Two different transformations will be used for the different sides
function transformColorsToSidesStandard(sideColors, allColors){
	transformedColors = "";
	for(var i = 8; i >= 0; i--){
		if(sideColors[i] === allColors[0]){
			transformedColors = transformedColors.concat("U");
		}else if(sideColors[i] === allColors[1]){
			transformedColors = transformedColors.concat("D");
		}else if(sideColors[i] === allColors[2]){
			transformedColors = transformedColors.concat("F");
		}else if(sideColors[i] === allColors[3]){
			transformedColors = transformedColors.concat("B");
		}else if(sideColors[i] === allColors[4]){
			transformedColors = transformedColors.concat("L");
		}else if(sideColors[i] === allColors[5]){
			transformedColors = transformedColors.concat("R");
		}
	}

	return transformedColors;
}

//Since some of the indexes in the chromes cube mismatch the indexes in the 
// twophase algo we need to do some complex transformation
//we need to treat the sideColors as an 2D array of colors
// the following function will apply the following transformation
// 0 1 2    2 5 8 
// 3 4 5 -> 1 4 7 	
// 6 7 8    0 3 6
//if we invert the loops the following transformation will be applied
// 0 1 2    6 3 0 
// 3 4 5 -> 7 4 1 	
// 6 7 8    8 5 2

function transformColorsToSidesComplex(sideColors, allColors, invertLoops){
	sideColors2D = [sideColors.slice(0, 3), sideColors.slice(3, 6), sideColors.slice(6, 9)];
	rows = sideColors2D.length;
	columns = sideColors2D[0].length;
	transformedColors = "";

	for (var column = 0; column < columns; column++){
		for(var row = 0; row < rows; row++){
			realRow = row;
			realCol = column;
			if(invertLoops){
				realRow = (rows-1) - realRow;
			}else{
				realCol = (columns-1) - realCol;
			}

			if(sideColors2D[realRow][realCol] === allColors[0]){
				transformedColors = transformedColors.concat("U");
			}else if(sideColors2D[realRow][realCol] === allColors[1]){
				transformedColors = transformedColors.concat("D");
			}else if(sideColors2D[realRow][realCol] === allColors[2]){
				transformedColors = transformedColors.concat("F");
			}else if(sideColors2D[realRow][realCol] === allColors[3]){
				transformedColors = transformedColors.concat("B");
			}else if(sideColors2D[realRow][realCol] === allColors[4]){
				transformedColors = transformedColors.concat("L");
			}else if(sideColors2D[realRow][realCol] === allColors[5]){
				transformedColors = transformedColors.concat("R");
			}
		}
	}

	return transformedColors;
}

//Function which transitions the current background color to a target rgb color over time
function colorTransition(targetR, targetG, targetB, error){
		if(targetR%5!=0 || targetG%5!=0 || targetB%5!=0 ){
			console.log("Color values are not mutiple of 5");
			clearInterval(gEvent)
		}
        if(currentR!=targetR || currentB!=targetB || currentG!=targetG){
        	document.getElementById("mainBody").style.backgroundColor="rgb("+currentR+","+currentG+","+currentB+")";	
        }else if(currentR==targetR&&currentG==targetG&&currentB==targetB){
        	//stop color transition
        	clearInterval(gEvent)
        	if(error){
				gEvent=setInterval("colorTransition("+r+","+g+","+b+", false);", 3);
        	}
        }
        changeColorValues(targetR, targetG, targetB);
}

//Increments or decrements te color values to match the target RGB
function changeColorValues(targetR, targetG, targetB){
	if(currentR < targetR){
		currentR += 5;
	}else if(currentR > targetR){
		currentR -= 5;
	}
	if(currentG < targetG){
		currentG += 5;
	}else if(currentG > targetG){
		currentG -= 5;
	}
	if(currentB < targetB){
		currentB += 5;
	}else if(currentB > targetB){
		currentB -= 5;
	}
}

//Resets the cube to a solved state
function resetCube(){

	// Create new cube and add listeners
	var controls = ERNO.Locked;

	var ua = navigator.userAgent,
		isIE = ua.indexOf('MSIE') > -1 || ua.indexOf('Trident/') > -1;

	window.cube = new ERNO.Cube({
		hideInvisibleFaces: true,
		controls: controls,
		renderer: isIE?ERNO.renderers.IeCSS3D : null

	});


	cube.addEventListener('click', function( event ){
		var stickerColor = '';
		var radios = document.getElementsByName('color');
		for(var i = 0; i < radios.length; i++){
			if (radios[i].checked){
				stickerColor = radios[i].id.toString();
			}
		}
		if(stickerColor != ''){
			var cubelet = event.detail.cubelet;
			if(cubelet.type != "center"){
				var face = event.detail.face;
				face = ( face === 'TOP' ) ? 'UP' : face
				var element = cubelet [face.toLowerCase()].element;
				var sticker = 'sticker ' + stickerColor;
				element.querySelector('.sticker').className = sticker
			};
		}
	});

	cube.camera.setLens(30);

	//Remove old cube and replace with the new one
	var container = document.getElementById('container');
	container.removeChild(container.childNodes[0]);
	container.appendChild(cube.domElement);

	//Reset the color picker
	var radios = document.getElementsByName('color');
	for(var i = 0; i < radios.length; i++){
		radios[i].checked = false;
	}

	//stop any current chenged to the background
	clearInterval(gEvent);
	//set background color to white
	gEvent=setInterval("colorTransition(255,255,255, false);", 10);

	// Reset the message
	var message = document.getElementById('outputMessage');
	message.innerHTML = "Scramble the cube or change colors using the color picker and then press solve";

}